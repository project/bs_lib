<?php

use Drupal\block\Entity\Block;

/**
 * Switch easing from not supported one to swing if needed.
 */
function bs_lib_post_update_fix_not_supported_easing() {
  $block_ids = \Drupal::entityQuery('block')->condition('plugin', 'bs_lib_scroll_to_top_block')->execute();
  $blocks = Block::loadMultiple($block_ids);
  foreach ($blocks as $block) {
    $settings = $block->get('settings');
    if (!in_array($settings['easing'], ['linear', 'swing'])) {
      $settings['easing'] = 'swing';
      $block->set('settings', $settings);
      $block->save();
    }
  }
}
