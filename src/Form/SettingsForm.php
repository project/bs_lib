<?php

namespace Drupal\bs_lib\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * bs_lib configuration form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_lib_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bs_lib.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bs_lib.settings')->get('anchor_scroll');

    $form['anchor_scroll'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Anchor scroll'),
      '#tree' => TRUE,
    ];

    $form['anchor_scroll']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Enable custom anchor scrolling functionality.'),
      '#default_value' => $config['enable'],
    ];

    $form['anchor_scroll']['offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset'),
      '#description' => $this->t('Enter offset value in pixels. This is vertical distance from top of a viewport to target scroll element.'),
      '#default_value' => $config['offset'],
      '#states' => [
        'visible' => [
          'input[name="anchor_scroll[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['anchor_scroll']['fixed_elements'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Fixed elements'),
      '#description' => $this->t('Enter fixed elements CSS selectors. Enter one per line. Element with highest vertical offset will be used for additional scroll offset. Use this for sticky elements like toolbar, sticky navigation etc.'),
      '#default_value' => join("\n", $config['fixed_elements']),
      '#states' => [
        'visible' => [
          'input[name="anchor_scroll[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['anchor_scroll']['exclude_links'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude links'),
      '#description' => $this->t('Enter exclude links CSS selectors. Enter one per line. For this links anchor scrolling will not be applied.'),
      '#default_value' => join("\n", $config['exclude_links']),
      '#states' => [
        'visible' => [
          'input[name="anchor_scroll[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('bs_lib.settings');
    $anchor_scroll = $config->get('anchor_scroll');
    $anchor_scroll['enable'] = $form_state->getValue(['anchor_scroll', 'enable']) === 1;
    $anchor_scroll['offset'] = (int) $form_state->getValue(['anchor_scroll', 'offset']);
    // Normalize new lines.
    $anchor_scroll['exclude_links'] = preg_split("(\r\n?|\n)", $form_state->getValue(['anchor_scroll', 'exclude_links']));
    $anchor_scroll['fixed_elements'] = preg_split("(\r\n?|\n)", $form_state->getValue(['anchor_scroll', 'fixed_elements']));
    $this->config('bs_lib.settings')
      ->set('anchor_scroll', $anchor_scroll)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
