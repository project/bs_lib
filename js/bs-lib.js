/**
 * @file
 */

// Bootstrap compiled JS assets are expecting global window.$ jQuery object. So
// we must define one first before starting to use Bootstrap JS.
window.$ = window.$ ? window.$ : window.jQuery;
